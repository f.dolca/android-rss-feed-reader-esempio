package com.example.rssfeedreaderesempio;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RssFeedAdapter
        extends RecyclerView.Adapter<RssItemViewHolder> {

    private List<RssFeedModel> mRssFeedModels;


    public RssFeedAdapter(List<RssFeedModel> rssFeedModels) {
        mRssFeedModels = rssFeedModels;
    }

    @Override
    public RssItemViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_rss_feed, parent, false);
        RssItemViewHolder holder = new RssItemViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RssItemViewHolder holder, int position) {
        final RssFeedModel rssFeedModel = mRssFeedModels.get(position);
        ((TextView)holder.view.findViewById(R.id.titleText)).setText(rssFeedModel.title);

        TextView desc = ((TextView)holder.view.findViewById(R.id.descriptionText));
        URLImageParser p = new URLImageParser(desc,holder.view.getContext());
        Spanned htmlSpan = Html.fromHtml(rssFeedModel.description, p, null);
        desc.setText(htmlSpan);

        ((TextView)holder.view.findViewById(R.id.linkText)).setText(rssFeedModel.link);

        holder.view.setOnClickListener(v -> {
            TextView linkView = (TextView)v.findViewById(R.id.linkText);
            System.out.println(linkView.getText().toString());

            Intent in = new Intent(holder.view.getContext(), BrowserActivity.class);
            String page_url = (linkView.getText().toString().trim());
            in.putExtra("url", page_url);
            holder.view.getContext().startActivity(in);
        });
    }

    @Override
    public int getItemCount() {
        return mRssFeedModels.size();
    }
}