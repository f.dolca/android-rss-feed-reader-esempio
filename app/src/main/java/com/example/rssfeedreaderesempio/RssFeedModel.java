package com.example.rssfeedreaderesempio;

public class RssFeedModel {
    public String title;
    public String link;
    public String description;
    public String pubDate;
    public String guid;

    public RssFeedModel(String title, String link, String description, String puDdate, String guid) {
        this.title = title;
        this.link = link;
        this.description = description;
        this.pubDate = puDdate;
        this.guid = guid;
    }
}
