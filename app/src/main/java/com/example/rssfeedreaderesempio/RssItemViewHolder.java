package com.example.rssfeedreaderesempio;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RssItemViewHolder extends RecyclerView.ViewHolder {
    //new String[]{ETag.LINK.value, ETag.TITLE.value, ETag.DESCRIPTION.value, ETag.PUB_DATE.value},
    TextView link;
    TextView title;
    TextView description;
    TextView pubDate;
    View view;

    public RssItemViewHolder(@NonNull View itemView) {
        super(itemView);
        this.link
                = (TextView)itemView
                .findViewById(R.id.linkText);
        this.title
                = (TextView)itemView
                .findViewById(R.id.title);
        this.description
                = (TextView)itemView
                .findViewById(R.id.descriptionText);

        view  = itemView;

    }
}
