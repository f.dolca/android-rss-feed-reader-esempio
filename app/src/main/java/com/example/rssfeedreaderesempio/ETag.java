package com.example.rssfeedreaderesempio;

public enum ETag {
    CHANNEL("channel"),
    TITLE("title"),
    LINK("link"),
    DESCRIPTION("description"),
    ITEM("item"),
    PUB_DATE("pubDate"),
    GUID("guid");

    public final String value;

    private ETag(String value) {
        this.value = value;
    }
}
